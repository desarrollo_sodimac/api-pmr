﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;

namespace API.Endpoints.Stores
{
    /// <summary>
    /// Store's Controllers
    /// </summary>
    public class StoresController : Gale.REST.RestController
    {
        /// <summary>
        /// Get all Locals from a specific country (ODATA Convention's)
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Swashbuckle.Swagger.Annotations.QueryableEndpoint()]
        [HierarchicalRoute("{country}")]
        public IHttpActionResult Get(string country)
        {
            var settings = new Gale.REST.Queryable.OData.Builders.GQLConfiguration();
            settings.filters.Add(new Gale.REST.Queryable.OData.Builders.GQLConfiguration.Filter()
            {
                field = "country_identifier",
                operatorAlias = "eq",
                value = country
            });
            return new Gale.REST.Http.HttpQueryableActionResult<Models.Local>(this.Request, settings);
        }


        /// <summary>
        /// Discover Locals , by Geo 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HierarchicalRoute("@{latitude:decimal},{longitude:decimal},{distance}km/Discover")]
        [Swashbuckle.Swagger.Annotations.SwaggerResponseRemoveDefaults]
        [Swashbuckle.Swagger.Annotations.SwaggerResponse(HttpStatusCode.OK)]
        public IHttpActionResult Discover(decimal latitude, decimal longitude, int distance, [FromUri]String q = null, [FromUri(Name = "$limit")]int limit = 10, [FromUri(Name = "$offset")]int offset = 0)
        {
            String user = Guid.NewGuid().ToString();
            return new Services.Discover(user, latitude, longitude, distance, q, limit, offset);
        }
    }
}