﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace API.Endpoints.Stores.Services
{
    /// <summary>
    /// Discover Locals near's
    /// </summary>
    public class Discover : Gale.REST.Http.HttpBaseActionResult
    {
        decimal _latitude;
        decimal _longitude;
        int _distance;
        string _user;
        string _query;
        int _limit;
        int _offset;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="latitude">Current Latitude to discover</param>
        /// <param name="longitude">Current Longitude to discover</param>
        /// <param name="distance">Distance to match</param>
        /// <param name="query">Query Name to match</param>
        /// <param name="limit">Records limit (Pagination)</param>
        /// <param name="offset">Records Offset (Pagination)</param>
        public Discover(String user, decimal latitude, decimal longitude, int distance, String query, int limit, int offset)
        {
            _user = user;
            _latitude = latitude;
            _longitude = longitude;
            _distance = distance;
            _query = query;
            _limit = limit;
            _offset = offset;
        }

        /// <summary>
        /// Async Process
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public override System.Threading.Tasks.Task<System.Net.Http.HttpResponseMessage> ExecuteAsync(System.Threading.CancellationToken cancellationToken)
        {
            using (Gale.Db.DataService svc = new Gale.Db.DataService("[PA_TIE_OBT_DescubrirLocales]"))
            {
                svc.Parameters.Add("ENTI_Token", _user);

                svc.Parameters.Add("Nombre", _query);
                svc.Parameters.Add("Distancia", _distance);
                svc.Parameters.Add("Latitud", _latitude);
                svc.Parameters.Add("Longitud", _longitude);
                svc.Parameters.Add("RegistrosPorPagina", _limit);
                svc.Parameters.Add("RegistrosSaltados", _offset);

                Gale.Db.EntityRepository rep = this.ExecuteQuery(svc);

                Models.Pagination pagination = rep.GetModel<Models.Pagination>(0).FirstOrDefault();
                List<Models.NearLocal> items = rep.GetModel<Models.NearLocal>(1);

                pagination.items = items;
                //----------------------------------------------------------------------------------------------------
                //Create Response
                var response = new HttpResponseMessage(System.Net.HttpStatusCode.OK)
                {
                    Content = new ObjectContent<Object>(
                       pagination,
                        System.Web.Http.GlobalConfiguration.Configuration.Formatters.KqlFormatter()
                    )
                };

                //Return Task
                return Task.FromResult(response);
                //----------------------------------------------------------------------------------------------------

            }
        }
    }
}