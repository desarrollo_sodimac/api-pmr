﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;

namespace API.Endpoints.Categories.Services
{
    public class Get : Gale.REST.Http.HttpBaseActionResult
    {
        String _country;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="country"></param>
        public Get(String country)
        {
            this._country = country;
        }

        public override System.Threading.Tasks.Task<System.Net.Http.HttpResponseMessage> ExecuteAsync(System.Threading.CancellationToken cancellationToken)
        {

            //------------------------------------------------------------------------------------
            //--[ Guard Exception's
            Gale.Exception.RestException.Guard(() => this._country == null, "EMPTY_COUNTRY", API.Errors.ResourceManager);
            Gale.Exception.RestException.Guard(() => this._country.Length != 2, "COUNTRY_BADFORMAT", API.Errors.ResourceManager);
            //------------------------------------------------------------------------------------

            using (Gale.Db.DataService svc = new Gale.Db.DataService("[PA_CAT_OBT_Categorias]"))
            {
                svc.Parameters.Add("CATE_Pais", _country);

                Gale.Db.EntityRepository rep = this.ExecuteQuery(svc);

                var items = rep.GetModel<Models.Categories.Category>();

                var menu = new List<Models.Categories.MenuItem>();


                //Find Categories Base (Parent is null)
                var categories = items.Where(cat => String.IsNullOrEmpty(cat.parent_atg));
                foreach (var item in categories)
                {
                    menu.Add(new Models.Categories.MenuItem()
                    {
                        descripcion = item.description,
                        id = item.atg,
                        imagen = item.image,
                        nombre = item.name,
                        orden = item.order,
                        subcategorias = GetSubcategories(item, items)
                    });
                }

                
                //----------------------------------------------------------------------------------------------------
                //Create Response
                var response = new HttpResponseMessage(System.Net.HttpStatusCode.OK)
                {
                    Content = new ObjectContent<Object>(
                        menu,
                        System.Web.Http.GlobalConfiguration.Configuration.Formatters.KqlFormatter()
                    )
                };

                //Max Age
                int maxAge = int.Parse(System.Configuration.ConfigurationManager.AppSettings["Gale:Cache:Minutes"]);
                response.Headers.CacheControl = new CacheControlHeaderValue()
                {
                    MaxAge = TimeSpan.FromMinutes(maxAge)
                };

                //Return Task
                return Task.FromResult(response);
                //----------------------------------------------------------------------------------------------------

            }
        }

        public List<Models.Categories.MenuItem> GetSubcategories(Models.Categories.Category parent, List<Models.Categories.Category> items)
        {
            List<Models.Categories.MenuItem> child_menu = new List<Models.Categories.MenuItem>();
            var childs = items.Where(it => it.parent_atg == parent.atg);
            foreach (var child in childs)
            {
                child_menu.Add(new Models.Categories.MenuItem()
                {
                    descripcion = child.description,
                    id = child.atg,
                    imagen = child.image,
                    nombre = child.name,
                    orden = child.order,
                    subcategorias = GetSubcategories(child, items)
                });
            }

            return child_menu;
        }
    }
}