﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using API.Endpoints.Categories.Models.Products;
using Newtonsoft.Json;

namespace API.Endpoints.Categories.Services.Products
{
    /// <summary>
    /// Get Productos from ATG
    /// </summary>
    public class Get : Gale.REST.Http.HttpBaseActionResult
    {
        String _country;
        String _category;
        String _orderBy;
        String _filters;

        int _store;
        int _offset;
        int _limit;


        /// <summary>
        /// 
        /// </summary>
        /// <param name="country"></param>
        /// <param name="category"></param>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        public Get(String country, int store, String category, String filters, String orderBy = "1", int offset = 0, int limit = 10)
        {
            this._country = country;
            this._store = store;
            this._category = category;
            this._offset = offset;
            this._limit = limit;
            this._orderBy = orderBy;
            this._filters = filters;
        }

        public override System.Threading.Tasks.Task<System.Net.Http.HttpResponseMessage> ExecuteAsync(System.Threading.CancellationToken cancellationToken)
        {

            //------------------------------------------------------------------------------------
            //--[ Guard Exception's
            Gale.Exception.RestException.Guard(() => this._country == null, "EMPTY_COUNTRY", API.Errors.ResourceManager);
            Gale.Exception.RestException.Guard(() => this._country.Length != 2, "COUNTRY_BADFORMAT", API.Errors.ResourceManager);
            Gale.Exception.RestException.Guard(() => this._category == null, "EMPTY_CATEGORY", API.Errors.ResourceManager);
            //------------------------------------------------------------------------------------

            //------------------------------------------------------------------------------------
            //--[ GET ATG TOKEN
            String ATG_token = API.Services.ATG.GetAuthorizationToken();
            //------------------------------------------------------------------------------------

            //------------------------------------------------------------------------------------
            //--[ Guard Exception's
            Gale.Exception.RestException.Guard(() => String.IsNullOrEmpty(ATG_token), "ATG_TOKEN_EMPTY", API.Errors.ResourceManager);
            //------------------------------------------------------------------------------------


            var products = GetProductsFromCategory(ATG_token);
            BuildProducts(ref products); //Add MetaData to Products

            Object result = products;


            //----------------------------------------------------------------------------------------------------
            //Create Response
            var response = new HttpResponseMessage(System.Net.HttpStatusCode.OK)
            {
                Content = new ObjectContent<Object>(
                    result,
                    System.Web.Http.GlobalConfiguration.Configuration.Formatters.KqlFormatter()
                )
            };
            //Max Age
            int maxAge = int.Parse(System.Configuration.ConfigurationManager.AppSettings["Gale:Cache:Minutes"]);
            response.Headers.CacheControl = new CacheControlHeaderValue()
            {
                MaxAge = TimeSpan.FromMinutes(maxAge)
            };

            //Return Task
            return Task.FromResult(response);
            //----------------------------------------------------------------------------------------------------


        }

        #region PMR & ATG EXTRACTION
        /// <summary>
        /// GET Products FROM THE ATG
        /// </summary>
        /// <returns></returns>
        private Models.Products.ProductList GetProductsFromCategory(String ATG_Token)
        {


            using (var client = new HttpClient())
            {
                String endpoint = System.Configuration.ConfigurationManager.AppSettings["Sodimac:ATG:Productos"];
                string json = JsonConvert.SerializeObject(new
                {
                    Header = new
                    {
                        country = _country,
                        commerce = System.Configuration.ConfigurationManager.AppSettings["Sodimac:ATG:Commerce"],
                        channel = System.Configuration.ConfigurationManager.AppSettings["Sodimac:ATG:Channel"]
                    },
                    Body = new
                    {
                        categoryId = _category,
                        facetsFlag = true,
                        orderFlag = true,
                        promoFlag = false,
                        productStart = _offset + 1,
                        productEnd = _offset + _limit,
                        orderBy = _orderBy,
                        filters = _filters.Split(',').ToArray()
                    }
                });

                // AUTHORIZATION AND SERVICE ID

                client.DefaultRequestHeaders.Add("Authorization", String.Format("Bearer {0}", ATG_Token));
                client.DefaultRequestHeaders.Add("idServicio", "ListaProductosObtener");
                client.Timeout = new TimeSpan(0, 1, 0);
                HttpResponseMessage Service = client.PostAsync(endpoint, new StringContent(json, Encoding.UTF8, "application/json")).Result;

                if (Service.IsSuccessStatusCode)
                {
                    //Wee need to fix the bad bad.. json dinamyc output from ATG....

                    var poorlyFormedJson = Service.Content.ReadAsStringAsync().Result;
                    var settings = new JsonSerializerSettings();

                    //Fixing ATG Poorly JSON with JSON Converter's
                    settings.Converters.Add(new API.Endpoints.Categories.Services.Products.Converters.ATG_JsonFixConverter<List<ATG_FacetAttribute>>());
                    settings.Converters.Add(new API.Endpoints.Categories.Services.Products.Converters.ATG_JsonFixConverter<List<ATG_FacetProperty>>());
                    settings.Converters.Add(new API.Endpoints.Categories.Services.Products.Converters.ATG_JsonFixConverter<List<ATG_productList>>());
                    settings.Converters.Add(new API.Endpoints.Categories.Services.Products.Converters.ATG_JsonFixConverter<List<ATG_ListPrice>>());



                    //Deserialize
                    var atg = JsonConvert.DeserializeObject<Models.Products.ATG_ResponseContent>(poorlyFormedJson, settings);


                    Models.Products.ProductList data = new ProductList();

                    #region FACET's
                    var facets = new List<Models.Products.Facet>();
                    if (atg.response.Body.listaProductosResponse.facets != null)
                    {
                        foreach (var facet in atg.response.Body.listaProductosResponse.facets.facet)
                        {
                            if (facet.attributes != null)
                            {
                                List<Models.Products.FacetAttribute> attributes = new List<FacetAttribute>();

                                //Prices
                                foreach (var attribute in facet.attributes)
                                {
                                    var filterId = attribute.filterCondition.Substring(attribute.filterCondition.IndexOf("N-") + 2);
                                    attributes.Add(new FacetAttribute()
                                    {
                                        nombre = filterId,
                                        valor = attribute.name,
                                        total = int.Parse(attribute.total)
                                    });
                                }

                                bool skip = true;
                                switch (facet.facetName)
                                {
                                    case "product.brand":
                                        facet.facetName = "Marca";
                                        skip = false;
                                        break;
                                    case "sku.price_plist_SEARCH_01":
                                        facet.facetName = "Precio";
                                        skip = false;
                                        break;
                                    case "specialFacet":
                                        facet.facetName = "Destacado";
                                        skip = false;
                                        break;
                                }
                                if (skip == false)
                                {
                                    facets.Add(new Models.Products.Facet()
                                    {
                                        atributos = attributes,
                                        nombre = facet.facetName,
                                    });
                                }
                            }

                        }
                    }
                    #endregion

                    #region SORTED LIST'S
                    var sortings = new List<Models.Products.Sorting>();
                    if (atg.response.Body.listaProductosResponse.sortList.items != null)
                    {
                        foreach (var sorting in atg.response.Body.listaProductosResponse.sortList.items)
                        {
                            if (sorting != null && sorting.properties != null)
                            {
                                sortings.Add(new Models.Products.Sorting()
                                {
                                    descripcion = sorting.properties.description,
                                    id = sorting.properties.id
                                });
                            }
                        }
                    }
                    #endregion

                    #region PRODUCT'S
                    var products = new List<Models.Products.Producto>();
                    if (atg.response.Body.listaProductosResponse.productList != null)
                    {
                        foreach (var product in atg.response.Body.listaProductosResponse.productList.items)
                        {
                            products.Add(new Models.Products.Producto()
                            {
                                sku = product.properties.productID
                            });
                        }
                    }
                    #endregion

                    data.productos = products;
                    data.criterios = sortings;
                    data.facetas = facets;

                    return data;
                }
                else
                {
                    throw new Gale.Exception.RestException("ATG_CONNECTION", Service.ReasonPhrase);
                }
            }

        }

        private void BuildProducts(ref Models.Products.ProductList productList)
        {

            List<String> skus = new List<string>();

            //Remove all C product's from the ATG Product List ,
            //Because , they mean "COMBO", "PROD" and in the "INTERNET APP" don' use
            productList.productos.RemoveAll((x) =>
            {
                return (
                    x.sku.IndexOf("c", StringComparison.OrdinalIgnoreCase) >= 0 ||
                    x.sku.IndexOf("prod", StringComparison.OrdinalIgnoreCase) >= 0
                );
            });

            productList.productos.ForEach((p) =>
            {
                skus.Add(p.sku);
            });

            List<API.Endpoints.Products.Models.PMR.Prices.PMR_Price> prices = GetPrices(skus);
            List<API.Endpoints.Products.Models.PMR.Products.PMR_Product> products = GetProductDetails(skus);
            List<API.Endpoints.Products.Models.ATG.Availability.ATG_metodosEnvioDespachoDisponiblesInternet> availabilities = GetAvailbilities(skus);

            productList.productos.ForEach((p) =>
            {
                var sku = p.sku;


                //FIND SINGLES ITEM'S
                API.Endpoints.Products.Models.PMR.Prices.PMR_Price price = prices.FirstOrDefault(item => item.sku == sku);
                API.Endpoints.Products.Models.PMR.Products.PMR_Product product = products.FirstOrDefault(item => item.sku == sku);
                API.Endpoints.Products.Models.ATG.Availability.ATG_metodosEnvioDespachoDisponiblesInternet availability = availabilities.FirstOrDefault(item => item.productID == sku);

                #region PRICES
                var precios_alternativos = new List<Models.Products.PreciosAlternativos>();
                precios_alternativos.Add(new Models.Products.PreciosAlternativos()
                {
                    tipo = "CMR",
                    valor = price.cmr
                });

                precios_alternativos.Add(new Models.Products.PreciosAlternativos()
                {
                    tipo = "INT",
                    valor = price.pi
                });
                #endregion

                #region MULTIMEDIAS
                var multimedias = new List<Models.Products.ItemMultimedia>();
                var image = product.imagenes.FirstOrDefault();
                if (image != null)
                {
                    multimedias.Add(new Models.Products.ItemMultimedia()
                    {
                        tipo = "IMAG",
                        url = image.imagen
                    });
                }
                #endregion

                p.multimedia = multimedias;
                p.nombre = product.nombre;
                p.nombreCorto = price.des;
                p.unidad = price.uv;
                p.stock = price.stk;
                p.disponibilidad = new ItemDisponibilidad()
                    {
                        despacho = availability.homeDelivery == "true",
                        retiro = availability.storePickup == "true",
                        tienda = availability.deliveryPoints == "true"
                    };
                p.precio = new Models.Products.Precio()
                {
                    normal = price.pl,
                    empleado = price.pemp,
                    alternativos = precios_alternativos
                };
                p.marca = new Models.Products.Marca()
                {
                    imagen = null,
                    nombre = product.marca
                };
                p.sku = price.sku;

            });

        }

        /// <summary>
        /// Obtiene los precios de los productos
        /// </summary>
        /// <param name="sku"></param>
        private List<API.Endpoints.Products.Models.PMR.Prices.PMR_Price> GetPrices(List<String> skus)
        {


            using (var client = new HttpClient())
            {
                String endpoint = System.Configuration.ConfigurationManager.AppSettings["Sodimac:PMR:ConsultaPrecio"];
                string json = JsonConvert.SerializeObject(new
                {
                    req = new
                    {
                        h = new
                        {
                            atr = new
                            {
                                p = _country,
                                e = System.Configuration.ConfigurationManager.AppSettings["Sodimac:PMR:Entity"],
                                t = _store,
                                a = System.Configuration.ConfigurationManager.AppSettings["Sodimac:PMR:Application"]
                            }
                        },
                        b = new
                        {
                            cod = skus.ToArray()
                        }
                    }
                });

                HttpResponseMessage Service = client.PostAsync(endpoint, new StringContent(json, Encoding.UTF8, "application/json")).Result;
                if (Service.IsSuccessStatusCode)
                {
                    var data = Service.Content.ReadAsAsync<API.Endpoints.Products.Models.PMR.Prices.PMR>().Result;

                    //----------------------------------------------------------------------------------------------------
                    //Guard Exception's
                    Gale.Exception.RestException.Guard(() => data.res.h.st.glosa != "OK", data.res.h.st.glosa, API.Errors.ResourceManager);
                    //----------------------------------------------------------------------------------------------------


                    return data.res.b.prod;
                }
                else
                {
                    throw new Gale.Exception.RestException("PMR_PRICES_SERVICE", Service.ReasonPhrase);
                }
            }


        }

        /// <summary>
        /// Obtiene los detalles de los productos
        /// </summary>
        /// <param name="sku"></param>
        private List<API.Endpoints.Products.Models.PMR.Products.PMR_Product> GetProductDetails(List<String> skus)
        {


            using (var client = new HttpClient())
            {
                String endpoint = System.Configuration.ConfigurationManager.AppSettings["Sodimac:PMR:ConsultaFichaProd"];
                string json = JsonConvert.SerializeObject(new
                {
                    req = new
                    {
                        h = new
                        {
                            atr = new
                            {
                                p = _country,
                                e = System.Configuration.ConfigurationManager.AppSettings["Sodimac:PMR:Entity"],
                                t = _store,
                                a = System.Configuration.ConfigurationManager.AppSettings["Sodimac:PMR:Application"]
                            }
                        },
                        b = new
                        {
                            sku = skus.ToArray()
                        }
                    }
                });

                HttpResponseMessage Service = client.PostAsync(endpoint, new StringContent(json, Encoding.UTF8, "application/json")).Result;
                if (Service.IsSuccessStatusCode)
                {
                    var data = Service.Content.ReadAsAsync<API.Endpoints.Products.Models.PMR.Products.PMR>().Result;

                    //----------------------------------------------------------------------------------------------------
                    //Guard Exception's
                    Gale.Exception.RestException.Guard(() => data.res.h.st.glosa != "OK", data.res.h.st.glosa, API.Errors.ResourceManager);
                    //----------------------------------------------------------------------------------------------------

                    return data.res.b.prod;
                }
                else
                {
                    throw new Gale.Exception.RestException("PMR_PRODUCTS_SERVICE", Service.ReasonPhrase);
                }
            }

        }


        /// <summary>
        /// Obtiene la disponiblidad de los productos
        /// </summary>
        /// <param name="skus"></param>
        /// <returns></returns>
        private List<API.Endpoints.Products.Models.ATG.Availability.ATG_metodosEnvioDespachoDisponiblesInternet> GetAvailbilities(List<String> skus)
        {

            //------------------------------------------------------------------------------------
            //--[ GET ATG TOKEN
            String ATG_token = API.Services.ATG.GetAuthorizationToken();
            //------------------------------------------------------------------------------------

            //------------------------------------------------------------------------------------
            //--[ Guard Exception's
            Gale.Exception.RestException.Guard(() => String.IsNullOrEmpty(ATG_token), "ATG_TOKEN_EMPTY", API.Errors.ResourceManager);
            //------------------------------------------------------------------------------------

            var availabilities = new List<API.Endpoints.Products.Models.ATG.Availability.ATG_metodosEnvioDespachoDisponiblesInternet>();

            #region GET PRODUCT AVAILABLITY
            skus.ForEach((sku) =>
            {
                //ATG fails when an SKU have a X,C in the ID ,so.... whe jump to a generic availability
                if (sku.IndexOf("x", StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    availabilities.Add(new Endpoints.Products.Models.ATG.Availability.ATG_metodosEnvioDespachoDisponiblesInternet()
                    {
                        deliveryPoints = "false",
                        homeDelivery = "false",
                        productID = sku,
                        storePickup = "false"
                    });
                    return;
                }

                using (var client = new HttpClient())
                {
                    String endpoint = System.Configuration.ConfigurationManager.AppSettings["Sodimac:ATG:Disponibilidad"];
                    string json = JsonConvert.SerializeObject(new
                    {
                        Header = new
                        {
                            country = _country,
                            commerce = System.Configuration.ConfigurationManager.AppSettings["Sodimac:ATG:Commerce"],
                            channel = System.Configuration.ConfigurationManager.AppSettings["Sodimac:ATG:Channel"]
                        },
                        Body = new
                        {
                            productId = sku
                        }
                    });

                    //ATG Header's
                    client.DefaultRequestHeaders.Add("Authorization", String.Format("Bearer {0}", ATG_token));
                    client.DefaultRequestHeaders.Add("idServicio", "MetodoEnvioDespachoObtener");
                    //client.Timeout = new TimeSpan(0, 1, 0);

                    HttpResponseMessage Service = client.PostAsync(endpoint, new StringContent(json, Encoding.UTF8, "application/json")).Result;
                    if (Service.IsSuccessStatusCode)
                    {
                        var data = Service.Content.ReadAsAsync<API.Endpoints.Products.Models.ATG.Availability.ATG_ResponseContent>().Result;

                        //----------------------------------------------------------------------------------------------------
                        //Guard Exception's
                        Gale.Exception.RestException.Guard(() => data.response.Header.message != "OK", data.response.Header.message, API.Errors.ResourceManager);
                        //----------------------------------------------------------------------------------------------------

                        availabilities.Add(data.response.Body.obtenerMetodosEnvioDespachoResponse.metodosEnvioDespachoDisponiblesInternet);
                    }
                    else
                    {
                        throw new Gale.Exception.RestException("PMR_AVAILABILITY_SERVICE", String.Format("SKU {0}: {1}", sku, Service.ReasonPhrase));
                    }
                }
            });
            #endregion

            return availabilities;
        }
        #endregion

    }
}