﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace API.Endpoints.Categories.Services.Products.Converters
{
    /// <summary>
    /// For fixing poorly JSON format.... (the json type is dinamic :S)
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ATG_JsonFixConverter<T> : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new NotImplementedException();
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            object retVal = new Object();
            if (reader.TokenType == JsonToken.StartObject)
            {
                Type type = typeof(T).GetGenericArguments()[0];

                var listType = typeof(List<>).MakeGenericType(type);
                Object instance = serializer.Deserialize(reader, type);

                var listInstance = Activator.CreateInstance(listType);
                (listInstance as System.Collections.IList).Add(instance);

                retVal = listInstance;
            }
            else if (reader.TokenType == JsonToken.StartArray)
            {
                existingValue = existingValue ?? serializer.ContractResolver.ResolveContract(objectType).DefaultCreator();
                serializer.Populate(reader, existingValue);

                return existingValue;
            }

            return retVal;
        }

        public override bool CanConvert(Type objectType)
        {
            return typeof(T).IsAssignableFrom(objectType);
        }
    }
}