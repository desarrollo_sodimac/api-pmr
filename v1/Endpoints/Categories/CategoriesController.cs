﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace API.Endpoints.Categories
{
    /// <summary>
    /// Categories Controller
    /// </summary>
    public class CategoriesController : Gale.REST.RestController
    {

        /// <summary>
        /// Get categories from a particular channel =)
        /// </summary>
        /// <param name="country">Country (2 character)</param>
        /// <returns></returns>
        [HierarchicalRoute("{country}")]
        public IHttpActionResult GetByCountry(string country)
        {
            return new Services.Get(country);
        }


        /// <summary>
        /// Get product from a particular category
        /// </summary>
        /// <param name="country">Country (2 character)</param>
        /// <param name="store">Store ID</param>
        /// <param name="category">Category ID</param>
        /// <param name="$offset">Pagination Offset</param>
        /// <param name="$limit">Pagination Limit</param>
        /// <param name="orderBy">Order By</param>
        /// <param name="filters">Filters (separated by comma)</param>
        /// <returns></returns>
        [HierarchicalRoute("{country}/{store}/{category}")]
        public IHttpActionResult GetProductsByCategory(string country, int store, string category, string orderBy = "1", string filters = "", [FromUri(Name = "$offset")]int offset = 0, [FromUri(Name = "$limit")]int limit = 10)
        {
            return new Services.Products.Get(country, store, category, filters, orderBy, offset, limit);
        }

    }
}