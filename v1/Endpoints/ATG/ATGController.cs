﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace API.Endpoints.ATG
{
    /// <summary>
    /// ATG Helper Controler (Hidden from the Explorer)
    /// </summary>
    [Obsolete]
    public class ATGController : Gale.REST.RestController
    {
        /// <summary>
        /// GET ATG Available token (Hide From the API)
        /// </summary>
        /// <returns></returns>
        [Obsolete]
        [HierarchicalRoute("Token")]
        public API.Endpoints.Categories.Models.ATG Get()
        {
            var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["Database:ConcentradorLatam"];
            Type ProviderType = typeof(Gale.Db.IDataActions).Assembly.GetType(connectionString.ProviderName);
            Gale.Db.IDataActions connection = (Activator.CreateInstance(ProviderType, new object[] { connectionString.ConnectionString }) as Gale.Db.IDataActions);

            using (Gale.Db.DataService svc = new Gale.Db.DataService("getToken"))
            {
                var repo = connection.ExecuteQuery(svc);
                var ATG = repo.GetModel<API.Endpoints.Categories.Models.ATG>().FirstOrDefault();

                //------------------------------------------------------------------------------------
                //--[ Guard Exception's
                Gale.Exception.RestException.Guard(() => String.IsNullOrEmpty(ATG.token), "ATG_TOKEN_EMPTY", API.Errors.ResourceManager);
                //------------------------------------------------------------------------------------

                return ATG;
            }
        }
    }
}