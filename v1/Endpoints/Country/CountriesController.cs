﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace API.Endpoints.Country
{
    /// <summary>
    /// Countries Controllers
    /// </summary>
    public class CountriesController : Gale.REST.RestController
    {

        /// <summary>
        /// Get all Countries (ODATA Convention's)
        /// </summary>
        /// <returns></returns>
        [Swashbuckle.Swagger.Annotations.QueryableEndpoint(typeof(Models.Country))]
        public IHttpActionResult Get()
        {
            return new Gale.REST.Http.HttpQueryableActionResult<Models.Country>(this.Request);
        }

    }
}