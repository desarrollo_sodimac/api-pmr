﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace API.Endpoints.Products
{
    /// <summary>
    /// Product Controller 
    /// </summary>
    public class ProductsController : Gale.REST.RestController
    {
        /// <summary>
        /// Get product details by SKU
        /// </summary>
        /// <param name="country">Country Identifier ('CL','PE','AR', etc)</param>
        /// <param name="local">Local Identifier</param>
        /// <param name="sku">Product SKU (without the '-')</param>
        /// <returns></returns>
        [HttpGet]
        [HierarchicalRoute("{country}/{local:int}/{sku}")]
        public IHttpActionResult FindBySKU(String country, Int32 local, String sku)
        {
            List<String> skus = new List<string>();
            skus.Add(sku);

            return new Services.FindBySKU(skus, country, local);
        }

        /// <summary>
        /// Get technical product details by SKU
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HierarchicalRoute("{country}/{local:int}/{sku}/ficha")]
        public IHttpActionResult GetTechnicalSheet(String country, Int32 local, String sku)
        {
            List<String> skus = new List<string>();
            skus.Add(sku);

            return new Services.GetTechnicalSheetBySKU(skus);
        }

        /// <summary>
        /// Get related products details by SKU
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HierarchicalRoute("{country}/{local:int}/{sku}/relacionados")]
        public IHttpActionResult GetRelatedProducts(String country, Int32 local, String sku)
        {
            List<String> skus = new List<string>();
            skus.Add(sku);

            return new Services.GetRelatedProductsBySKU(skus);
        }


        /// <summary>
        /// Get related products details by SKU
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [HierarchicalRoute("{country}/{local:int}/{sku}/promociones")]
        public Object GetPromotions(String country, Int32 local, String sku)
        {
            var multimedias = new List<Models.ItemMultimedia>();
            multimedias.Add(new Models.ItemMultimedia()
            {
                tipo = "IMG",
                url = "https://www.cmrfalabella.com/b2cfapr/CMRCORP/grafica/imgs/CL/icoBenefOU.jpg"
            });

            List<Object> promotions = new List<object>();
            promotions.Add(new Models.Producto()
            {
                descripcion = "",
                marca = new Models.Marca()
                {
                    imagen = "http://sodimac.scene7.com/is/image/SodimacCL/6300",
                    nombre = "Inchalam"
                },
                multimedia = multimedias,
                nombre = "Producto en Promoción 1",
                nombreCorto = "Producto 1",
                disponibilidad = new Models.ItemDisponibilidad()
                {
                    despacho = true,
                    retiro = false,
                    tienda = false
                },
                unidad = "c/u",
                stock = 0,
                precio = new Models.Precio()
                {
                    normal = 123123,
                    empleado = 11000,
                    alternativos = new List<Models.PreciosAlternativos>()
                },
                puntos = new Models.ItemPunto()
                {
                    cmr = -1
                },
                sku = "20202"
            });
            return promotions;

        }

    }
}