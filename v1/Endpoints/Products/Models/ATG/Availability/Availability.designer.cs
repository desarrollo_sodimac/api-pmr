﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34209
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace API.Endpoints.Products.Models.ATG.Availability
{
	using System.Data.Linq;
	using System.Data.Linq.Mapping;
	using System.Data;
	using System.Collections.Generic;
	using System.Reflection;
	using System.Linq;
	using System.Linq.Expressions;
	using System.ComponentModel;
	using System;
	
	
	public partial class AvailabilityDataContext : System.Data.Linq.DataContext
	{
		
		private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
		
    #region Extensibility Method Definitions
    partial void OnCreated();
    #endregion
		
		public AvailabilityDataContext(string connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public AvailabilityDataContext(System.Data.IDbConnection connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public AvailabilityDataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public AvailabilityDataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public System.Data.Linq.Table<ATG_ResponseContent> ATG_ResponseContents
		{
			get
			{
				return this.GetTable<ATG_ResponseContent>();
			}
		}
		
		public System.Data.Linq.Table<ATG_Response> ATG_Responses
		{
			get
			{
				return this.GetTable<ATG_Response>();
			}
		}
		
		public System.Data.Linq.Table<ATG_Header> ATG_Headers
		{
			get
			{
				return this.GetTable<ATG_Header>();
			}
		}
		
		public System.Data.Linq.Table<ATG_Body> ATG_Bodies
		{
			get
			{
				return this.GetTable<ATG_Body>();
			}
		}
		
		public System.Data.Linq.Table<ATG_metodosEnvioDespachoDisponiblesTienda> ATG_metodosEnvioDespachoDisponiblesTiendas
		{
			get
			{
				return this.GetTable<ATG_metodosEnvioDespachoDisponiblesTienda>();
			}
		}
		
		public System.Data.Linq.Table<ATG_metodosEnvioDespachoDisponiblesInternet> ATG_metodosEnvioDespachoDisponiblesInternets
		{
			get
			{
				return this.GetTable<ATG_metodosEnvioDespachoDisponiblesInternet>();
			}
		}
		
		public System.Data.Linq.Table<ATG_obtenerMetodosEnvioDespachoResponse> ATG_obtenerMetodosEnvioDespachoResponses
		{
			get
			{
				return this.GetTable<ATG_obtenerMetodosEnvioDespachoResponse>();
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="")]
	public partial class ATG_ResponseContent
	{
		
		private ATG_Response _response;
		
		public ATG_ResponseContent()
		{
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_response", CanBeNull=false)]
		public ATG_Response response
		{
			get
			{
				return this._response;
			}
			set
			{
				if ((this._response != value))
				{
					this._response = value;
				}
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="")]
	public partial class ATG_Response
	{
		
		private ATG_Header _Header;
		
		private ATG_Body _Body;
		
		public ATG_Response()
		{
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Header", CanBeNull=false)]
		public ATG_Header Header
		{
			get
			{
				return this._Header;
			}
			set
			{
				if ((this._Header != value))
				{
					this._Header = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_Body", CanBeNull=false)]
		public ATG_Body Body
		{
			get
			{
				return this._Body;
			}
			set
			{
				if ((this._Body != value))
				{
					this._Body = value;
				}
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="")]
	public partial class ATG_Header
	{
		
		private string _code;
		
		private string _message;
		
		public ATG_Header()
		{
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_code", CanBeNull=false)]
		public string code
		{
			get
			{
				return this._code;
			}
			set
			{
				if ((this._code != value))
				{
					this._code = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_message", CanBeNull=false)]
		public string message
		{
			get
			{
				return this._message;
			}
			set
			{
				if ((this._message != value))
				{
					this._message = value;
				}
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="")]
	public partial class ATG_Body
	{
		
		private ATG_obtenerMetodosEnvioDespachoResponse _listaCategoriasResponse;
		
		public ATG_Body()
		{
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Name="listaCategoriasResponse", Storage="_listaCategoriasResponse", CanBeNull=false)]
		public ATG_obtenerMetodosEnvioDespachoResponse obtenerMetodosEnvioDespachoResponse
		{
			get
			{
				return this._listaCategoriasResponse;
			}
			set
			{
				if ((this._listaCategoriasResponse != value))
				{
					this._listaCategoriasResponse = value;
				}
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="")]
	public partial class ATG_metodosEnvioDespachoDisponiblesTienda
	{
		
		public ATG_metodosEnvioDespachoDisponiblesTienda()
		{
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="")]
	public partial class ATG_metodosEnvioDespachoDisponiblesInternet
	{
		
		private string _productID;
		
		private string _homeDelivery;
		
		private string _storePickup;
		
		private string _deliveryPoints;
		
		public ATG_metodosEnvioDespachoDisponiblesInternet()
		{
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_productID", CanBeNull=false)]
		public string productID
		{
			get
			{
				return this._productID;
			}
			set
			{
				if ((this._productID != value))
				{
					this._productID = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_homeDelivery", CanBeNull=false)]
		public string homeDelivery
		{
			get
			{
				return this._homeDelivery;
			}
			set
			{
				if ((this._homeDelivery != value))
				{
					this._homeDelivery = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_storePickup", CanBeNull=false)]
		public string storePickup
		{
			get
			{
				return this._storePickup;
			}
			set
			{
				if ((this._storePickup != value))
				{
					this._storePickup = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_deliveryPoints", CanBeNull=false)]
		public string deliveryPoints
		{
			get
			{
				return this._deliveryPoints;
			}
			set
			{
				if ((this._deliveryPoints != value))
				{
					this._deliveryPoints = value;
				}
			}
		}
	}
	
	[global::System.Data.Linq.Mapping.TableAttribute(Name="")]
	public partial class ATG_obtenerMetodosEnvioDespachoResponse
	{
		
		private ATG_metodosEnvioDespachoDisponiblesTienda _metodosEnvioDespachoDisponiblesTienda;
		
		private ATG_metodosEnvioDespachoDisponiblesInternet _metodosEnvioDespachoDisponiblesInternet;
		
		public ATG_obtenerMetodosEnvioDespachoResponse()
		{
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_metodosEnvioDespachoDisponiblesTienda", CanBeNull=false)]
		public ATG_metodosEnvioDespachoDisponiblesTienda metodosEnvioDespachoDisponiblesTienda
		{
			get
			{
				return this._metodosEnvioDespachoDisponiblesTienda;
			}
			set
			{
				if ((this._metodosEnvioDespachoDisponiblesTienda != value))
				{
					this._metodosEnvioDespachoDisponiblesTienda = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_metodosEnvioDespachoDisponiblesInternet", CanBeNull=false)]
		public ATG_metodosEnvioDespachoDisponiblesInternet metodosEnvioDespachoDisponiblesInternet
		{
			get
			{
				return this._metodosEnvioDespachoDisponiblesInternet;
			}
			set
			{
				if ((this._metodosEnvioDespachoDisponiblesInternet != value))
				{
					this._metodosEnvioDespachoDisponiblesInternet = value;
				}
			}
		}
	}
}
#pragma warning restore 1591
