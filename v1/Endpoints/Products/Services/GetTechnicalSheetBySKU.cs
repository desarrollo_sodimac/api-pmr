﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;

namespace API.Endpoints.Products.Services
{
    /// <summary>
    /// Get technical Sheet for a list o product's
    /// </summary>
    public class GetTechnicalSheetBySKU : Gale.REST.Http.HttpReadActionResult<List<String>>
    {

        /// <summary>
        /// Consctructor
        /// </summary>
        /// <param name="skus"></param>
        public GetTechnicalSheetBySKU(List<String> skus) : base(skus) { }

        /// <summary>
        /// Execute Async
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public override System.Threading.Tasks.Task<System.Net.Http.HttpResponseMessage> ExecuteAsync(System.Threading.CancellationToken cancellationToken)
        {
            //----------------------------------------------------------------------------------------------------
            //Guard Exception's
            Gale.Exception.RestException.Guard(() => this.Model == null, "EMPTY_PRODUCT", API.Errors.ResourceManager);
            //----------------------------------------------------------------------------------------------------

            List<Models.FichaProducto> products = new List<Models.FichaProducto>();

            this.Model.ForEach((sku) =>
            {

                List<Models.AtributoFichaProducto> attributes = new List<Models.AtributoFichaProducto>();


                //DUMYY
                attributes.Add(new Models.AtributoFichaProducto()
                {
                    nombre = "Accesorios",
                    valor = "75 Unidades"
                });

                attributes.Add(new Models.AtributoFichaProducto()
                {
                    nombre = "Categoría",
                    valor = "Herramientas eléctricas"
                });

                attributes.Add(new Models.AtributoFichaProducto()
                {
                    nombre = "Empaque",
                    valor = "Caja"
                });

                products.Add(new Models.FichaProducto()
                {
                    sku = sku,
                    atributos = attributes
                });
            });
         
            //----------------------------------------------------------------------------------------------------
            //Guard Exception's
            Gale.Exception.RestException.Guard(() => products.Count == 0, "PRODUCTS_NOT_FOUND", API.Errors.ResourceManager);
            //----------------------------------------------------------------------------------------------------

            //----------------------------------------------------------------------------------------------------
            //Send First ot array if more than one SKU searched
            Object result = null;
            if (products.Count > 1)
            {
                result = products;
            }
            else
            {
                result = products.FirstOrDefault();
            }
            //----------------------------------------------------------------------------------------------------

            //----------------------------------------------------------------------------------------------------
            //Create Response
            var response = new HttpResponseMessage(System.Net.HttpStatusCode.OK)
            {
                Content = new ObjectContent<Object>(
                    result,
                    System.Web.Http.GlobalConfiguration.Configuration.Formatters.KqlFormatter()
                )
            };

            //Return Task
            return Task.FromResult(response);
            //----------------------------------------------------------------------------------------------------


        }
    }
}