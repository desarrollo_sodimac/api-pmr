﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;

namespace API.Endpoints.Products.Services
{
    public class FindBySKU : Gale.REST.Http.HttpReadActionResult<List<String>>
    {
        private Int32 _local;
        private String _country;

        /// <summary>
        /// Consctructor
        /// </summary>
        /// <param name="skus"></param>
        public FindBySKU(List<String> skus, String country, Int32 local)
            : base(skus)
        {
            _country = country;
            _local = local;
        }

        /// <summary>
        /// Execute Async
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public override System.Threading.Tasks.Task<System.Net.Http.HttpResponseMessage> ExecuteAsync(System.Threading.CancellationToken cancellationToken)
        {
            //----------------------------------------------------------------------------------------------------
            //Guard Exception's
            Gale.Exception.RestException.Guard(() => this.Model == null, "EMPTY_PRODUCTS", API.Errors.ResourceManager);
            //----------------------------------------------------------------------------------------------------

            //----------------------------------------------------------------------------------------------------
            // THE QUERY IS SPLITTED IN TWO PMR SERVICES :P
            List<Models.Producto> products = BuildProducts(this.Model);
            //----------------------------------------------------------------------------------------------------

            //----------------------------------------------------------------------------------------------------
            //Guard Exception's
            Gale.Exception.RestException.Guard(() => products.Count == 0, "PRODUCT_NOT_FOUND", API.Errors.ResourceManager);
            //----------------------------------------------------------------------------------------------------

            //----------------------------------------------------------------------------------------------------
            //Send First ot array if more than one SKU searched
            Object result = null;
            if (products.Count > 1)
            {
                result = products;
            }
            else
            {
                result = products.FirstOrDefault();
            }
            //----------------------------------------------------------------------------------------------------

            //----------------------------------------------------------------------------------------------------
            //Create Response
            var response = new HttpResponseMessage(System.Net.HttpStatusCode.OK)
            {
                Content = new ObjectContent<Object>(
                    result,
                    System.Web.Http.GlobalConfiguration.Configuration.Formatters.KqlFormatter()
                )
            };

            //Return Task
            return Task.FromResult(response);
            //----------------------------------------------------------------------------------------------------


        }

        #region PMR & ATG EXTRACTION
        private List<Models.Producto> BuildProducts(List<String> skus)
        {
            List<Models.Producto> buildProducts = new List<Models.Producto>();

            List<Models.PMR.Prices.PMR_Price> prices = GetPrices(skus);
            List<Models.PMR.Products.PMR_Product> products = GetProductDetails(skus);
            List<Models.ATG.Availability.ATG_metodosEnvioDespachoDisponiblesInternet> availabilities = GetAvailbilities(skus);

            skus.ForEach((sku) =>
            {
                //FIND SINGLES ITEM'S
                Models.PMR.Prices.PMR_Price price = prices.FirstOrDefault(item => item.sku == sku);
                Models.PMR.Products.PMR_Product product = products.FirstOrDefault(item => item.sku == sku);
                Models.ATG.Availability.ATG_metodosEnvioDespachoDisponiblesInternet availability = availabilities.FirstOrDefault(item => item.productID == sku);


                #region PRICES
                var precios_alternativos = new List<Models.PreciosAlternativos>();
                precios_alternativos.Add(new Models.PreciosAlternativos()
                {
                    tipo = "CMR",
                    valor = price.cmr
                });

                precios_alternativos.Add(new Models.PreciosAlternativos()
                {
                    tipo = "INT",
                    valor = price.pi
                });
                #endregion

                #region MULTIMEDIAS
                var multimedias = new List<Models.ItemMultimedia>();
                product.imagenes.ForEach((imagen) =>
                {
                    multimedias.Add(new Models.ItemMultimedia()
                    {
                        tipo = "IMAG",
                        url = imagen.imagen
                    });
                });

                /*
                //ADD DUMMY VIDEO
                multimedias.Add(new Models.ItemMultimedia()
                {
                    tipo = "VIDE",
                    url = "https://www.youtube.com/watch?v=FDLB0GMBXvQ"
                });
                 * */

                #endregion

                #region EXTRAS
                string api_url = System.Web.HttpContext.Current.Request.Url.AbsoluteUri;
                string version = Gale.REST.Config.GaleConfig.apiVersion;
                string hateoas = api_url + (api_url.EndsWith("/") ? "" : "/") + version;
                List<Models.ItemExtra> extras = new List<Models.ItemExtra>();

                extras.Add(new Models.ItemExtra()
                {
                    nombre = "Ficha Técnica",
                    link = new Models.HateOAS_Link()
                    {
                        href = hateoas + "ficha",
                        rel = "self",
                        method = "GET"
                    },
                    identificador = "FTEC"
                });

                extras.Add(new Models.ItemExtra()
                {
                    nombre = "Productos Relacionados",
                    link = new Models.HateOAS_Link()
                    {
                        href = hateoas + "relacionados",
                        rel = "self",
                        method = "GET"
                    },
                    identificador = "PCOM"
                });

                extras.Add(new Models.ItemExtra()
                {
                    nombre = "Promociones",
                    link = new Models.HateOAS_Link()
                    {
                        href = hateoas + "promociones",
                        rel = "self",
                        method = "GET"
                    },
                    identificador = "PROM"
                });

                #endregion

                buildProducts.Add(new Models.Producto()
                {
                    extras = extras,
                    descripcion = "",
                    marca = new Models.Marca()
                    {
                        imagen = null,
                        nombre = product.marca
                    },
                    multimedia = multimedias,
                    nombre = product.nombre,
                    nombreCorto = price.des,
                    pasillo = product.pasillo,
                    disponibilidad = new Models.ItemDisponibilidad()
                    {
                        despacho = availability.homeDelivery == "true",
                        retiro = availability.storePickup == "true",
                        tienda = availability.deliveryPoints == "true"
                    },
                    unidad = price.uv,
                    stock = price.stk,
                    precio = new Models.Precio()
                    {
                        normal = price.pl,
                        empleado = price.pemp,
                        alternativos = precios_alternativos
                    },
                    puntos = new Models.ItemPunto()
                    {
                        cmr = -1
                    },
                    sku = price.sku
                });

            });

            return buildProducts;
        }

        /// <summary>
        /// Obtiene los precios de los productos
        /// </summary>
        /// <param name="sku"></param>
        private List<Models.PMR.Prices.PMR_Price> GetPrices(List<String> skus)
        {


            using (var client = new HttpClient())
            {
                String endpoint = System.Configuration.ConfigurationManager.AppSettings["Sodimac:PMR:ConsultaPrecio"];
                string json = JsonConvert.SerializeObject(new
                {
                    req = new
                    {
                        h = new
                        {
                            atr = new
                            {
                                p = _country,
                                e = System.Configuration.ConfigurationManager.AppSettings["Sodimac:PMR:Entity"],
                                t = _local,
                                a = System.Configuration.ConfigurationManager.AppSettings["Sodimac:PMR:Application"]
                            }
                        },
                        b = new
                        {
                            cod = skus.ToArray()
                        }
                    }
                });

                HttpResponseMessage Service = client.PostAsync(endpoint, new StringContent(json, Encoding.UTF8, "application/json")).Result;
                if (Service.IsSuccessStatusCode)
                {
                    var data = Service.Content.ReadAsAsync<Models.PMR.Prices.PMR>().Result;

                    //----------------------------------------------------------------------------------------------------
                    //Guard Exception's
                    Gale.Exception.RestException.Guard(() => data.res.h.st.glosa != "OK", data.res.h.st.glosa, API.Errors.ResourceManager);
                    //----------------------------------------------------------------------------------------------------


                    return data.res.b.prod;
                }
                else
                {
                    throw new Gale.Exception.RestException("PMR_PRICES_SERVICE", Service.ReasonPhrase);
                }
            }


        }

        /// <summary>
        /// Obtiene los detalles de los productos
        /// </summary>
        /// <param name="sku"></param>
        private List<Models.PMR.Products.PMR_Product> GetProductDetails(List<String> skus)
        {


            using (var client = new HttpClient())
            {
                String endpoint = System.Configuration.ConfigurationManager.AppSettings["Sodimac:PMR:ConsultaFichaProd"];
                string json = JsonConvert.SerializeObject(new
                {
                    req = new
                    {
                        h = new
                        {
                            atr = new
                            {
                                p = _country,
                                e = System.Configuration.ConfigurationManager.AppSettings["Sodimac:PMR:Entity"],
                                t = _local,
                                a = System.Configuration.ConfigurationManager.AppSettings["Sodimac:PMR:Application"]
                            }
                        },
                        b = new
                        {
                            sku = skus.ToArray()
                        }
                    }
                });

                HttpResponseMessage Service = client.PostAsync(endpoint, new StringContent(json, Encoding.UTF8, "application/json")).Result;
                if (Service.IsSuccessStatusCode)
                {
                    var data = Service.Content.ReadAsAsync<Models.PMR.Products.PMR>().Result;

                    //----------------------------------------------------------------------------------------------------
                    //Guard Exception's
                    Gale.Exception.RestException.Guard(() => data.res.h.st.glosa != "OK", data.res.h.st.glosa, API.Errors.ResourceManager);
                    //----------------------------------------------------------------------------------------------------

                    return data.res.b.prod;
                }
                else
                {
                    throw new Gale.Exception.RestException("PMR_PRODUCTS_SERVICE", Service.ReasonPhrase);
                }
            }

        }

        /// <summary>
        /// Obtiene la disponiblidad de los productos
        /// </summary>
        /// <param name="skus"></param>
        /// <returns></returns>
        private List<Models.ATG.Availability.ATG_metodosEnvioDespachoDisponiblesInternet> GetAvailbilities(List<String> skus)
        {

            //------------------------------------------------------------------------------------
            //--[ GET ATG TOKEN
            String ATG_token = API.Services.ATG.GetAuthorizationToken();
            //------------------------------------------------------------------------------------

            //------------------------------------------------------------------------------------
            //--[ Guard Exception's
            Gale.Exception.RestException.Guard(() => String.IsNullOrEmpty(ATG_token), "ATG_TOKEN_EMPTY", API.Errors.ResourceManager);
            //------------------------------------------------------------------------------------

            var availabilities = new List<Models.ATG.Availability.ATG_metodosEnvioDespachoDisponiblesInternet>();

            #region GET PRODUCT AVAILABLITY
            skus.ForEach((sku) =>
            {
                //ATG fails when an SKU have a X in the ID ,so.... whe jump to a generic availability
                if (sku.IndexOf("x", StringComparison.OrdinalIgnoreCase) >= 0)
                {
                    availabilities.Add(new Models.ATG.Availability.ATG_metodosEnvioDespachoDisponiblesInternet()
                    {
                        deliveryPoints = "false",
                        homeDelivery = "false",
                        productID = sku,
                        storePickup = "false"
                    });
                    return;
                }


                using (var client = new HttpClient())
                {
                    String endpoint = System.Configuration.ConfigurationManager.AppSettings["Sodimac:ATG:Disponibilidad"];
                    string json = JsonConvert.SerializeObject(new
                    {
                        Header = new
                        {
                            country = _country,
                            commerce = System.Configuration.ConfigurationManager.AppSettings["Sodimac:ATG:Commerce"],
                            channel = System.Configuration.ConfigurationManager.AppSettings["Sodimac:ATG:Channel"]
                        },
                        Body = new
                        {
                            productId = sku,
                        }
                    });

                    //ATG Header's
                    client.DefaultRequestHeaders.Add("Authorization", String.Format("Bearer {0}", ATG_token));
                    client.DefaultRequestHeaders.Add("idServicio", "MetodoEnvioDespachoObtener");
                    client.Timeout = new TimeSpan(0, 1, 0);

                    HttpResponseMessage Service = client.PostAsync(endpoint, new StringContent(json, Encoding.UTF8, "application/json")).Result;
                    if (Service.IsSuccessStatusCode)
                    {
                        var data = Service.Content.ReadAsAsync<Models.ATG.Availability.ATG_ResponseContent>().Result;

                        //----------------------------------------------------------------------------------------------------
                        //Guard Exception's
                        Gale.Exception.RestException.Guard(() => data.response.Header.message != "OK", data.response.Header.message, API.Errors.ResourceManager);
                        //----------------------------------------------------------------------------------------------------

                        availabilities.Add(data.response.Body.obtenerMetodosEnvioDespachoResponse.metodosEnvioDespachoDisponiblesInternet);
                    }
                    else
                    {
                        throw new Gale.Exception.RestException("ATG_AVAILABILITY_SERVICE", String.Format("SKU: {0} - {1} - {2} - {3}", sku, Service.ReasonPhrase, endpoint, ATG_token));
                    }
                }
            });
            #endregion

            return availabilities;
        }
        #endregion
    }
}