﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;

namespace API.Endpoints.Products.Services
{
    /// <summary>
    /// Get Related Products, associated by a list of Products
    /// </summary>
    public class GetRelatedProductsBySKU : Gale.REST.Http.HttpReadActionResult<List<String>>
    {

        /// <summary>
        /// Consctructor
        /// </summary>
        /// <param name="skus"></param>
        public GetRelatedProductsBySKU(List<String> skus) : base(skus) { }

        /// <summary>
        /// Execute Async
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public override System.Threading.Tasks.Task<System.Net.Http.HttpResponseMessage> ExecuteAsync(System.Threading.CancellationToken cancellationToken)
        {
            //----------------------------------------------------------------------------------------------------
            //Guard Exception's
            Gale.Exception.RestException.Guard(() => this.Model == null, "EMPTY_PRODUCT", API.Errors.ResourceManager);
            //----------------------------------------------------------------------------------------------------

            List<Models.ResultadoProducto> products = new List<Models.ResultadoProducto>();
            this.Model.ForEach((sku) =>
            {
                var related = new List<Models.ProducctoRelacionado>();

                related.Add(new Models.ProducctoRelacionado()
                {
                    nombre = "Producto Relacionado",
                    sku = "33434",
                    precio = new Models.Precio()
                    {
                        normal = 29292,
                        empleado = 12828
                    },
                    nombreCorto = "prod relat",
                    unidad = "c/u",
                    stock = 0
                });

                related.Add(new Models.ProducctoRelacionado()
                {
                    nombre = "Producto Relacionado 2",
                    sku = "33434",
                    precio = new Models.Precio()
                    {
                         normal = 928292,
                          empleado = 82828
                    },
                    nombreCorto = "prod relat ",
                    unidad = "c/u",
                    stock = 1
                });

                products.Add(new Models.ResultadoProducto()
                {
                    sku = sku,
                    relacionados = related
                });

            });

            //----------------------------------------------------------------------------------------------------
            //Guard Exception's
            Gale.Exception.RestException.Guard(() => products.Count == 0, "PRODUCTS_NOT_FOUND", API.Errors.ResourceManager);
            //----------------------------------------------------------------------------------------------------

            //----------------------------------------------------------------------------------------------------
            //Send First ot array if more than one SKU searched
            Object result = null;
            if (products.Count > 1)
            {
                result = products;
            }
            else
            {
                result = products.FirstOrDefault();
            }
            //----------------------------------------------------------------------------------------------------

            //----------------------------------------------------------------------------------------------------
            //Create Response
            var response = new HttpResponseMessage(System.Net.HttpStatusCode.OK)
            {
                Content = new ObjectContent<Object>(
                    result,
                    System.Web.Http.GlobalConfiguration.Configuration.Formatters.KqlFormatter()
                )
            };

            //Return Task
            return Task.FromResult(response);
            //----------------------------------------------------------------------------------------------------


        }
    }
}