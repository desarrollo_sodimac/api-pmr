﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using RestSharp;

namespace API.Services
{
    public class ATG
    {
        private static bool canDirectConnectToDB = true;

        public static String GetAuthorizationToken()
        {
            #region GET A VALID TOKEN FOR THE ATG CONNECTION
            try
            {

                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["Database:ConcentradorLatam"];
                Type ProviderType = typeof(Gale.Db.IDataActions).Assembly.GetType(connectionString.ProviderName);
                Gale.Db.IDataActions connection = (Activator.CreateInstance(ProviderType, new object[] { connectionString.ConnectionString }) as Gale.Db.IDataActions);

                using (Gale.Db.DataService svc = new Gale.Db.DataService("getToken"))
                {
                    var repo = connection.ExecuteQuery(svc);
                    var ATG = repo.GetModel<API.Endpoints.Categories.Models.ATG>().FirstOrDefault();
                    ATG.origin = "AZURE DATABASE";

                    //------------------------------------------------------------------------------------
                    //--[ Guard Exception's
                    Gale.Exception.RestException.Guard(() => String.IsNullOrEmpty(ATG.token), "ATG_TOKEN_EMPTY", API.Errors.ResourceManager);
                    //------------------------------------------------------------------------------------

                    return ATG.token;
                }
            }
            catch (Exception ex)
            {
                return TryGetToAPI().token;
            }
            #endregion
        }

        private static API.Endpoints.Categories.Models.ATG TryGetToAPI()
        {
            try
            {
                var baseEndpoint = System.Configuration.ConfigurationManager.AppSettings["PMR:Endpoint:BaseAddress"];
                var client = new RestClient(baseEndpoint);
                var request = new RestRequest("ATG/Token", Method.GET);

                var ATG = client.Execute<API.Endpoints.Categories.Models.ATG>(request).Data;
                ATG.origin = "WEB API";

                canDirectConnectToDB = false;

                return ATG;
            }
            catch (Exception ex)
            {
                throw new Gale.Exception.RestException("CONCENTRADOR_EXCEPTION", ex.Message);
            }
        }
    }

}